

  // pegando os módulos:
require('ssl-root-cas').inject();
var fs = require('fs');
var http = require('http');
var https = require('https');
var express = require('express');
var bodyParser = require('body-parser');
var appEx = express();
var PeerServer = require('peer').ExpressPeerServer;


  // criando o servidor Express e PeerServer:
var privateKey  = fs.readFileSync('rafael.openssl', 'utf8');
var certificate = fs.readFileSync('rafael.cer', 'utf8');
var server = https.createServer({key: privateKey, cert: certificate}, appEx);
//var server = http.createServer(appEx);
server.listen(9000);

//===========================================================
var ids = {};
//ids['eu'] = true;
appEx.use(bodyParser.json());

appEx.post('/loginCheck', function(req,res,next){
  if( req.body && req.body.login ){
    if( !ids[req.body.login] ){
      res.send({ status: true });
      return;
    }
  }
  res.send({ status: false });
});
appEx.post('/login', function(req,res,next){
  if( req.body.login && !ids[req.body.login] ){
    ids[req.body.login] = true;
    res.send({ status: true });
    return;
  }
  res.send({ status: false });
});
appEx.post('/logout', function(req,res,next){
  delete ids[req.body.login];
});
appEx.get('/lista', function(req,res,next){
  res.send({ ids: ids });
});
//===========================================================


appEx.use('/stream', PeerServer(server, {debug: true}) );
appEx.use('/', express.static('public_html'));



console.log('---  Servidor iniciado');
