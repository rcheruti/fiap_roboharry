M.controller('Chat',['$scope','$http','$state','$stateParams','Sessao','$element',
    function($scope,$http,$state,$stateParams,Sessao,$element){
  
  if( !$stateParams.peerId ){
    $state.go('lobby');
    return;
  }
  
  Sessao.addScope($scope);
  $scope.camOn = false;
  var stream, streamRe,
    video = $element.find('video').eq(1),
    videoRe = $element.find('video').eq(0);
  
  $scope.meuId = Sessao.getId();
  $scope.peerId = $stateParams.peerId;
  $scope.sala = Sessao.getSalas()[$stateParams.peerId];
  
  $scope.msg = '';
  $scope.send = function(){
    $scope.sala.conn.send( $scope.msg );
    $scope.sala.msgs.push({ msg: $scope.msg, peerId: $scope.meuId });
    $scope.msg = '';
  };
  
  $scope.voltar = function(){
    $state.go('lobby');
  };
  
  function _onCall(call){
    console.log('onCall : chamada de Stream remoto');
    navigator.getUserMedia({ video: true, audio: true }, function(streamOn){
      stream = streamOn;
      video[0].src = window.URL.createObjectURL(streamOn);
      $scope.camOn = true;
      console.log('onCall : respondendo com meu Stream');
      call.answer(streamOn);
      call.on('stream',function(remoteStream){
        console.log('onCall : retorno do Stream remoto');
        streamRe = remoteStream;
        videoRe[0].src = window.URL.createObjectURL(remoteStream);
        if(!$scope.$$phase) $scope.$apply();
      });
      if(!$scope.$$phase) $scope.$apply();
    }, function(err){});
    
  }
  Sessao.onCall(_onCall);
  $scope.$on('$destroy',function(){
    Sessao.removeOnCall(_onCall);
  });
  $scope.camera = function(){
    navigator.getUserMedia({ video: true, audio: true }, function(streamOn){
      stream = streamOn;
      video[0].src = window.URL.createObjectURL(streamOn);
      $scope.camOn = true;
      if(!streamRe){
        console.log('Sessao.call');
        Sessao.call($scope.peerId, streamOn).on('stream',function(remoteStream){
          console.log('Sessao.call : retorno do Stream remoto');
          streamRe = remoteStream;
          videoRe[0].src = window.URL.createObjectURL(remoteStream);
          if(!$scope.$$phase) $scope.$apply();
        });
      }
      if(!$scope.$$phase) $scope.$apply();
    }, function(err){});
  };
  $scope.cameraDesligar = function(){
    if(stream){
      streamRe = null;
      var tracks = stream.getTracks();
      for(var g in tracks){
        if( tracks[g].stop ) tracks[g].stop();
      }
      if(stream.stop) stream.stop();
    }
    $scope.camOn = false;
  };
  
}]);