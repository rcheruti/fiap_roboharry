M.controller('Lobby',['$scope','$http','Sessao','$state',
    function($scope,$http,Sessao,$state){
  
  if( !Sessao.getId() ){
    $state.go('login');
    return;
  }
  
  $scope.lista = [];
  var meuId = Sessao.getId();
  
  $scope.chamar = function(id){
    if( Sessao.conectarPeer(id) ) $state.go('chat', { peerId: id });
  };
  
  $scope.refresh = function(){
    $http.get('/lista').then(function(data){
      var ids = data.data.ids;
      if( ids ){
        delete ids[meuId];
        $scope.lista = Object.keys(ids);
      }
    });
  };
  $scope.refresh();
  
}]);