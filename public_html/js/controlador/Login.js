M.controller('Login',['$scope','$http','$timeout','$state','Sessao',
    function($scope,$http,$timeout,$state,Sessao){
  
  console.log($scope);
  $scope.loginCheck = 0;
  
  var lastCall = null;
  $scope.check = function(){
    if(lastCall) $timeout.cancel(lastCall);
    lastCall = $timeout(function(){
      lastCall = null;
      if( !$scope.login ){
        $scope.loginCheck = 0;
        return;
      }
      $scope.loginCheck = 0;
      $http.post('/loginCheck',{ login: $scope.login }).then(function(data){
        $scope.loginCheck = data.data.status? 1 : -1;
      });
    }, 200);
  };
  $scope.entrar = function(){
    if( $scope.loginCheck === 0 ){
      $scope.msg = 'Escreva um nome de usuário';
      limparMsg();
      return;
    }
    if( $scope.loginCheck < 0 ){
      $scope.msg = 'Esse nome já está em uso';
      limparMsg();
      return;
    }
    var login = $scope.login;
    $http.post('/login',{ login: login }).then(function(data){
      if( data.data.status ){
        Sessao.entrar(login);
        $state.go('lobby');
      }else{
        $scope.msg = 'Esse nome já está em uso';
        limparMsg();
      }
    });
  };
  
  var limparTimer = null;
  function limparMsg(){
    if( limparTimer ) $timeout.cancel(limparTimer);
    limparTimer = $timeout(function(){
      limparTimer = null;
      $scope.msg = '';
    }, 5000);
  }
  
}]);