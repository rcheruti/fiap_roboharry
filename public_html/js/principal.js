
var M = angular.module('PeerJs',['ngTouch','ui.router']);

M.run(['$http','Sessao','$window',
    function($http,Sessao,$window){
  
  $window.addEventListener('beforeunload', function(){
    if( Sessao.getId() ){
      $http.post('/logout', {login: Sessao.getId()});
    }
  });
  
}]);
