
$(function(){
  
  var configPeer = {
                      //host: '127.0.0.1', 
                      host: '192.168.0.13', 
                      port: 9000, 
                      path: '/stream', 
                      debug: 0  //  [ 0, 1, 2, 3 ]
                    };
  var peer = new Peer(configPeer);
  
  peer.on('open', function(id){
    $('.idSessao span').text(id);
  });
  var connData = null;
  var msgBox = $('.msgBox');
  
  $('#novaSessao').on('click', openSessao);
  $('#juntarSessao').on('click', joinSessao);
  $('.enviar').on('click', function(){
    if( connData ){
      connData.send( msgBox.val() );
      msgBox.val('');
    }else{
      console.log('connData', connData);
    }
  });
  
  function openSessao(){
    if( peer ){
      peer.disconnect();
      peer.destroy();
    }
    peer = new Peer('id_open', configPeer);
    
    peer.on('open', function(id){
      $('.idSessao span').text(id);
    });
    peer.on('connection', function(conn){
      tratarConn(conn);
    });
    
    peer.on('call', function(call){
      tratarMediaStream(peer, call);
    });
  }
  
  
  function joinSessao(){
    var conn = peer.connect('id_open');
    tratarConn(conn);
    tratarMediaStream(peer);
  }
  
  function tratarConn(conn){
    connData = conn;
    console.log('Nova Conexão: ' + conn.peer);
    conn.on('data', function(data){
      _txt(data);
    });
  }
  function _txt(msg){
    var span = $('.spanMsg').clone(true);
      $('.msg').append( span.text(msg) );
  }
  
  function tratarMediaStream(peer, call){
    navigator.getUserMedia({ video: true, audio: true }, function(stream){
      _txt('getUserMedia carregado!');
      $('#video')[0].src = window.URL.createObjectURL(stream);
      
      if(!call){
        _txt('"call" pedido!');
        call = peer.call('id_open', stream);
      }else{
        _txt('Resposta para "call" enviada!');
        call.answer(stream);
      }
      
      call.on('stream', function(remoteStream){
        try{
          _txt('Evento "stream" chegou!');
          $('#remoteVideo')[0].src = window.URL.createObjectURL(remoteStream);
        }catch(err){
          _txt('Erro em remoteStream .createObjectURL: ' + err);
        }
      });
    }, function(err){ 
      console.log('Erro em .getUserMedia: ', err); 
      _txt('Erro em .getUserMedia: ' + err);
    });
  }
  
  
  //=========================================================
  
  /*
    navigator.getUserMedia({ video: true, audio: true }, function(stream){
      $('#video')[0].src = window.URL.createObjectURL(stream);
      
      var call = peer.call('idSessao', stream);
      if( call ){
        call.on('stream', function(remoteStream){
          $('#remoteVideo')[0].src = window.URL.createObjectURL(remoteStream);
        });
      }
    }, function(err){ 
      console.log(err); 
    });
   */
  
});

