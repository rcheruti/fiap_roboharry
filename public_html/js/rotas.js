M.config(['$urlRouterProvider','$stateProvider',
    function($urlRouterProvider,$stateProvider){
  
  $urlRouterProvider.otherwise('login');
  
  $stateProvider.state('login',{
    url:'/login', views:{ conteudo:{ templateUrl:'login.html' } }
  }).state('lobby',{
    url:'/lobby', views:{ conteudo:{ templateUrl:'lobby.html' } }
  }).state('chat',{
    url:'/chat', views:{ conteudo:{ templateUrl:'chat.html' } },
    params: { peerId: null }
  });
  
}]);