M.service('Sessao',['$location',function($location){
  
  var peer = null;
  var meuId = null;
  var salas = {};
  var scopes = {};
  var onCallFns = [];
  
  // Modelo de msg: { msg, peerId }
  function _addSala(id, conn){
    var sala = {
      conn: conn,
      msgs: []
    };
    salas[id] = sala;
    conn.on('data',function(msg){
      sala.msgs.push({ msg: msg, peerId: conn.peer });
      for(var g in scopes){
        if( !scopes[g].$$phase ) scopes[g].$apply();
      }
    });
    return sala;
  }
  
  return {
    
    addScope: function($scope){
      scopes[$scope.$id] = $scope;
      $scope.$on('$destroy',function(){
        delete scopes[$scope.$id];
      });
    },
    
    getId: function(){ return meuId; },
    
    entrar: function(id){
      if( peer ){
        peer.disconnect();
        peer.destroy();
      }
      meuId = id;
      peer = new Peer(id, { 
                    host: $location.host(),
                    port: $location.port(),
                    path:'/stream', 
                    secure: true, 
                    debug: 0 // [0,1,2,3]
      });
      peer.on('connection', function(conn){
        _addSala(conn.peer, conn);
      });
      peer.on('call',function(call){
        for(var g in onCallFns){
          onCallFns[g](call);
        }
        /*
        getUserMedia({video: true, audio: true}, function(stream) {
          call.answer(stream); // Answer the call with an A/V stream.
          call.on('stream', function(remoteStream) {
            // Show stream in some video/canvas element.
          });
        }, function(err) {
          console.log('Failed to get local stream' ,err);
        });
        /* */
      });
    },
    
    onCall: function(fn){
      onCallFns.push(fn);
    },
    removeOnCall: function(fn){
      for(var i = 0; i < onCallFns.length; i++){
        if( onCallFns[i] === fn ){
          onCallFns.splice(i,1);
          break;
        }
      }
    },
    
    sair: function(){
      if( peer ){
        peer.disconnect();
        peer.destroy();
      }
      salas = {};
      meuId = null;
    },
    
    conectarPeer: function(id){
      var conn = salas[id];
      if( conn && conn.conn.open ) return conn;
      conn = peer.connect(id);
      if(conn){
        return _addSala(id, conn);
      }
      return null;
    },
    desconectarPeer: function(id){
      var conn = salas[id];
      conn.close();
      delete salas[id];
    },
    
    call: function(id, stream){
      var call = peer.call(id, stream);
      return call;
    },
    
    getSalas: function(){
      return salas;
    },
    
  };
  
}]);